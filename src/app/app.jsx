import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/root.jsx';

//require('./components/root.jsx');
//let div = document.createElement('div');
//div.style = "height:100%; width: 100%";
//document.body.appendChild(div);
//ReactDOM.render(<Root/>, div);

var div = document.querySelector('.scene-container')
if (div == null) {
    div = document.createElement('div');
    document.body.appendChild(div);
}

console.log('DIV = ' + div);
ReactDOM.render(<Root/>, div);