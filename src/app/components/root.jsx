import 'aframe';
import 'babel-polyfill';
import {Animation, Entity, Scene} from 'aframe-react';
import React from 'react';

import Camera from './Camera.jsx';
import Cursor from './Cursor.jsx';
import Sky from './Sky.jsx';

// <Camera><Cursor/></Camera>
class Root extends React.Component {
    render () {
        return (
            <Scene>
                <Camera>

                </Camera>

                <Sky/>

                <Entity light={{type: 'ambient', color: '#888'}}/>
                <Entity light={{type: 'directional', intensity: 0.5}} position={[-1, 1, 0]}/>
                <Entity light={{type: 'directional', intensity: 1}} position={[1, 1, 0]}/>

                <Entity geometry="primitive: box; depth:1.5, height:1.5,width:6" material='color: red'
                        position={[0, -1, -2]}/>


                <div>
                    <h1>Hello World!!</h1>
                </div>

            </Scene>
        );
    }
}
export default Root;
